use std::net::Ipv4Addr;
use std::path::PathBuf;
use std::process::exit;
use std::{net::SocketAddr, str::FromStr};

use apply::Apply;
use sm_api::datastore::datastore_server::DatastoreServer;
use tonic::{transport::Server, Status};
use tracing::{error, info};

use crate::service::DatastoreService;

mod service;

pub(crate) const PORT_ENV: &str = "SM_DATASTORE_PORT";
pub(crate) const PORT_DEFAULT: u16 = 50051;

pub(crate) const IP_ENV: &str = "SM_DATASTORE_IP";
pub(crate) const IP_DEFAULT: Ipv4Addr = Ipv4Addr::new(127, 0, 0, 1);

pub(crate) const DATABASE_PATH_ENV: &str = "SM_DATASTORE_DATABASE_PATH";
pub(crate) const DATABASE_PATH_DEFAULT: &str = "/var/lib/sm-datastore";

pub(crate) const LOG_LEVEL_ENV: &str = "SM_DATASTORE_LOG_LEVEL";
pub(crate) const LOG_LEVEL_DEFAULT: tracing::Level = tracing::Level::WARN;

pub(crate) const LOG_SPAN: &str = "sm-datastore";

#[derive(Clone, Debug)]
struct AppError(String);

pub fn env_or_default<T: FromStr>(env: &str, default: T) -> T {
    std::env::var(env)
        .ok()
        .and_then(|l| l.parse().ok())
        .unwrap_or(default)
}

#[tokio::main]
async fn main() -> Result<(), tonic::transport::Error> {
    let log_level = env_or_default(LOG_LEVEL_ENV, LOG_LEVEL_DEFAULT);
    let ip = env_or_default(IP_ENV, IP_DEFAULT);
    let port = env_or_default(PORT_ENV, PORT_DEFAULT);
    let database_path = env_or_default(DATABASE_PATH_ENV, PathBuf::from(DATABASE_PATH_DEFAULT));

    tracing_subscriber::fmt().with_max_level(log_level).init();

    let addr = SocketAddr::from((ip, port));

    let datastore_svc = ::datastore::open(database_path)
        .unwrap_or_else(|e| {
            error!("{}", AppError::from(&e).0);
            exit(1)
        })
        .apply(DatastoreService)
        .apply(DatastoreServer::new);

    info!(%addr, "starting server");

    Server::builder()
        .trace_fn(|_| tracing::info_span!(LOG_SPAN))
        .add_service(datastore_svc)
        .serve(addr)
        .await
        .map_err(|e| {
            error!("{}", AppError::from(&e).0);
            exit(1)
        })
}

impl<T: std::error::Error> From<T> for AppError {
    fn from(err: T) -> Self {
        let mut msg = format!("Error: {err}");
        let mut source = err.source();
        while let Some(e) = source {
            msg.push_str(&format!("\nCaused by: {e}"));
            source = e.source();
        }

        Self(msg.trim().into())
    }
}

impl From<AppError> for Status {
    fn from(e: AppError) -> Self {
        error!("{}", e.0);
        Self::internal(e.0)
    }
}
