use datastore::Db;
use sm_api::datastore::*;
use tonic::{Request, Response, Status};
use tracing::{debug, info, instrument};

use crate::AppError;

use datastore::TreeExt;

#[derive(Clone, Debug)]
pub struct DatastoreService(pub Db);

#[tonic::async_trait]
impl sm_api::datastore::datastore_server::Datastore for DatastoreService {
    #[instrument(skip_all)]
    async fn get(&self, request: Request<GetRequest>) -> Result<Response<GetReply>, Status> {
        let GetRequest { tree, key } = request.into_inner();

        debug!(%tree, ?key, "retrieving value for key");

        let value = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .get(&key)
            .map_err(AppError::from)?
            .map(|i| i.to_vec());

        info!(%tree, ?key, "retrieved value for key");

        Ok(Response::new(GetReply { value }))
    }

    #[instrument(skip_all)]
    async fn get_all(
        &self,
        request: Request<GetAllRequest>,
    ) -> Result<Response<GetAllReply>, Status> {
        let GetAllRequest { tree } = request.into_inner();

        debug!(%tree, "retrieving all entries");

        let items = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .get_all()
            .map_err(AppError::from)?
            .into_iter()
            .map(|(k, v)| sm_api::datastore::KvPair {
                key: k.to_vec(),
                value: v.to_vec(),
            })
            .collect::<Vec<_>>();

        info!(%tree, "retrieved {} entries",items.len());

        Ok(Response::new(GetAllReply { items }))
    }

    #[instrument(skip_all)]
    async fn keys(&self, request: Request<KeysRequest>) -> Result<Response<KeysReply>, Status> {
        let KeysRequest { tree } = request.into_inner();

        debug!(%tree, "retrieving all keys");

        let keys = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .keys()
            .map_err(AppError::from)?
            .into_iter()
            .map(|k| k.to_vec())
            .collect::<Vec<_>>();

        info!(%tree, "retrieved {} keys",keys.len());

        Ok(Response::new(KeysReply { keys }))
    }

    #[instrument(skip_all)]
    async fn values(
        &self,
        request: Request<ValuesRequest>,
    ) -> Result<Response<ValuesReply>, Status> {
        let ValuesRequest { tree } = request.into_inner();

        debug!(%tree, "retrieving all values");

        let values = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .values()
            .map_err(AppError::from)?
            .into_iter()
            .map(|k| k.to_vec())
            .collect::<Vec<_>>();

        info!(%tree, "retrieved {} values",values.len());

        Ok(Response::new(ValuesReply { values }))
    }

    #[instrument(skip_all)]
    async fn insert(
        &self,
        request: Request<InsertRequest>,
    ) -> Result<Response<InsertReply>, Status> {
        let InsertRequest { tree, key, value } = request.into_inner();

        debug!(%tree, "inserting entry");

        // true if key was new
        let previous_value = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .insert(&key, value)
            .map_err(AppError::from)?
            .map(|v| v.to_vec());

        info!(%tree, ?key, ?previous_value, "inserted entry");

        Ok(Response::new(InsertReply { previous_value }))
    }

    #[instrument(skip_all)]
    async fn insert_key(
        &self,
        request: Request<InsertKeyRequest>,
    ) -> Result<Response<InsertKeyReply>, Status> {
        let InsertKeyRequest { tree, key } = request.into_inner();

        debug!(%tree, "inserting key");

        // true if key was new
        let previous_value = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .insert_key(&key)
            .map_err(AppError::from)?
            .map(|v| v.to_vec());

        info!(%tree, ?key, ?previous_value, "inserted key");

        Ok(Response::new(InsertKeyReply { previous_value }))
    }

    #[instrument(skip_all)]
    async fn insert_value(
        &self,
        request: Request<InsertValueRequest>,
    ) -> Result<Response<InsertValueReply>, Status> {
        let InsertValueRequest { tree, value } = request.into_inner();

        debug!(%tree, "inserting value");

        self.0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .insert_value(value)
            .map_err(AppError::from)?;

        info!(%tree, "inserted value");

        Ok(Response::new(InsertValueReply {}))
    }

    #[instrument(skip_all)]
    async fn get_or_insert(
        &self,
        request: Request<GetOrInsertRequest>,
    ) -> Result<Response<GetOrInsertReply>, Status> {
        let GetOrInsertRequest {
            tree,
            key,
            default_value,
        } = request.into_inner();

        debug!(%tree, ?key, "retrieving value or inserting default");

        let value = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .get_or_insert(&key, default_value.clone())
            .map_err(AppError::from)?
            .to_vec();

        if value == default_value {
            info!(%tree, ?key, "using default value");
        } else {
            info!(%tree, ?key, "retrieved value");
        }

        Ok(Response::new(GetOrInsertReply { value }))
    }

    #[instrument(skip_all)]
    async fn swap(&self, request: Request<SwapRequest>) -> Result<Response<SwapReply>, Status> {
        let SwapRequest { tree, key1, key2 } = request.into_inner();

        debug!(%tree, ?key1, ?key2, "swapping values for keys");

        let swapped = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .swap(&key1, &key2)
            .map_err(AppError::from)?;

        info!(%tree, ?key1, ?key2, %swapped, "swapped values for keys");

        Ok(Response::new(SwapReply { swapped }))
    }

    #[instrument(skip_all)]
    async fn remove(
        &self,
        request: Request<RemoveRequest>,
    ) -> Result<Response<RemoveReply>, Status> {
        let RemoveRequest { tree, key } = request.into_inner();

        debug!(%tree, ?key, "removing entry");

        let previous_value = self
            .0
            .open_tree(&tree)
            .map_err(AppError::from)?
            .remove(&key)
            .map_err(AppError::from)?
            .map(|v| v.to_vec());

        info!(%tree, ?key, ?previous_value, "removed entry");

        Ok(Response::new(RemoveReply { previous_value }))
    }
}
