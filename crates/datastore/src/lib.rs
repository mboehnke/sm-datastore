#[cfg(test)]
mod tests;

use std::path::Path;

use sled::{transaction::TransactionalTree, Config, IVec};

pub use sled::{transaction::TransactionError, Db, Error, Tree};

pub const FLUSH_EVERY_MS: Option<u64> = Some(1000);
pub const CACHE_CAPACITY: u64 = 500_000_000;
pub const USE_COMPRESSION: bool = true;
pub const COMPRESSION_FACTOR: i32 = 10;
pub const PRINT_PROFILE_ON_DROP: bool = false;

pub fn open(path: impl AsRef<Path>) -> Result<Db, Error> {
    custom_config().path(path).open()
}

fn custom_config() -> Config {
    sled::Config::new()
        .cache_capacity(CACHE_CAPACITY)
        .use_compression(USE_COMPRESSION)
        .compression_factor(COMPRESSION_FACTOR)
        .print_profile_on_drop(PRINT_PROFILE_ON_DROP)
        .flush_every_ms(FLUSH_EVERY_MS)
}

pub trait TreeExt {
    /// Retrieve all key/value pairs from the `Tree`.
    fn get_all(&self) -> Result<Vec<(IVec, IVec)>, Error>;

    /// Retrieve all keys from the `Tree`.
    fn keys(&self) -> Result<Vec<IVec>, Error>;

    /// Retrieve all values from the `Tree`.
    fn values(&self) -> Result<Vec<IVec>, Error>;

    /// Insert a key to an empty value, returning the last value if it was set.
    fn insert_key(&self, key: impl AsRef<[u8]>) -> Result<Option<IVec>, Error>;

    /// Insert a value into the `Tree`.
    ///
    /// This generates an ID and uses it as key.
    /// IDs are guaranteed to be unique but may still clash with manually defined keys.
    fn insert_value(&self, value: impl Into<IVec>) -> Result<(), TransactionError>;

    /// Retrieve a value from the `Tree` if it exists.
    ///
    /// Otherwise insert the default value and return that.
    fn get_or_insert(
        &self,
        key: impl AsRef<[u8]>,
        default_value: impl Into<IVec>,
    ) -> Result<IVec, Error>;

    /// Swap the values of two keys, returning true if any changes were made.
    ///
    /// If one or both key(s) don't exist, this will result in a deletion.
    fn swap(
        &self,
        key1: impl AsRef<[u8]>,
        key2: impl AsRef<[u8]>,
    ) -> Result<bool, TransactionError>;
}

impl TreeExt for Tree {
    fn get_all(&self) -> Result<Vec<(IVec, IVec)>, Error> {
        self.iter().collect()
    }

    fn keys(&self) -> Result<Vec<IVec>, Error> {
        self.iter().map(|res| res.map(|(k, _v)| k)).collect()
    }

    fn values(&self) -> Result<Vec<IVec>, Error> {
        self.iter().map(|res| res.map(|(_k, v)| v)).collect()
    }

    fn insert_key(&self, key: impl AsRef<[u8]>) -> Result<Option<IVec>, Error> {
        self.insert(key, &[])
    }

    fn insert_value(&self, value: impl Into<IVec>) -> Result<(), TransactionError> {
        let value = value.into();
        self.transaction(|tree| {
            // retry until we find an 'unused' id
            loop {
                let key = tree.generate_id().map(u64::to_be_bytes)?;
                if tree.get(&key)?.is_some() {
                    continue;
                }
                tree.insert(&key, &value)?;
                break;
            }
            Ok(())
        })
    }

    fn get_or_insert(
        &self,
        key: impl AsRef<[u8]>,
        default_value: impl Into<IVec>,
    ) -> Result<IVec, Error> {
        if let Some(value) = self.get(&key)? {
            return Ok(value);
        }
        let value = default_value.into();
        self.insert(key, &value)?;
        Ok(value)
    }

    fn swap(
        &self,
        key1: impl AsRef<[u8]>,
        key2: impl AsRef<[u8]>,
    ) -> Result<bool, TransactionError> {
        let (key1, key2) = (key1.as_ref(), key2.as_ref());
        self.transaction(|tree: &TransactionalTree| {
            let insert = |key, value| {
                if let Some(v) = value {
                    tree.insert(key, v).map(|_| true)
                } else {
                    Ok(false)
                }
            };
            let value1 = tree.remove(key1)?;
            let value2 = tree.remove(key2)?;
            let inserted1 = insert(key1, value2)?;
            let inserted2 = insert(key2, value1)?;
            Ok(inserted1 || inserted2)
        })
    }
}
