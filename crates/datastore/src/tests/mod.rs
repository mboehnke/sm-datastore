// todo: implement tests for Tree::insert_key, Tree::insert_value, Tree::get_or_insert

use std::collections::{HashMap, HashSet};

use assay::assay;
use rand::{prelude::SliceRandom, thread_rng};
use sled::Db;

use crate::TreeExt;

mod data;

#[assay]
fn open_new() {
    crate::open("ds")?;
}

#[assay]
fn open_existing() {
    let (mut data, ds) = gen_db(&["tree"], 256);
    ds.flush()?;
    drop(ds);
    let ds = crate::open("ds")?;
    let tree = ds.open_tree("tree")?;
    let data_r = tree.get_all()?;
    assert_eq!(data.len(), data_r.len());
    for (key, value) in data_r {
        let expected = data.remove(&key.to_vec()).unwrap();
        assert_eq!(value, expected)
    }
    assert!(data.is_empty());
}

#[assay]
fn get_all() {
    let (mut data, ds) = gen_db(&["tree"], 256);
    let tree = ds.open_tree("tree")?;
    let data_r = tree.get_all()?;
    assert_eq!(data.len(), data_r.len());
    for (key, value) in data_r {
        let expected = data.remove(&key.to_vec()).unwrap();
        assert_eq!(value, expected)
    }
    assert!(data.is_empty());
}

#[assay]
fn keys() {
    let (data, ds) = gen_db(&["tree"], 256);
    let mut keys = data.keys().collect::<HashSet<_>>();
    let data_r = ds.open_tree("tree")?.keys()?;
    assert_eq!(data.len(), data_r.len());
    for key in data_r {
        assert_eq!(keys.remove(&key.to_vec()), true)
    }
}

#[assay]
fn values() {
    let (data, ds) = gen_db(&["tree"], 256);
    let mut values = data.values().collect::<HashSet<_>>();
    let data_r = ds.open_tree("tree")?.values()?;
    assert_eq!(data.len(), data_r.len());
    for value in data_r {
        assert_eq!(values.remove(&value.to_vec()), true)
    }
}

#[assay]
fn swap() {
    let (mut data, ds) = gen_db(&["tree"], 256);
    let mut keys = data.keys().cloned().collect::<Vec<_>>();
    let mut rng = thread_rng();
    keys.shuffle(&mut rng);
    let tree = ds.open_tree("tree")?;
    for ks in keys.chunks(2) {
        let key1 = ks[0].to_vec();
        let key2 = ks[1].to_vec();
        let val1 = data.remove(&key1);
        let val2 = data.remove(&key2);
        tree.swap(&key1, &key2)?;
        let r1 = tree.remove(key1)?.map(|v| v.to_vec());
        let r2 = tree.remove(key2)?.map(|v| v.to_vec());
        assert_eq!(val1, r2);
        assert_eq!(val2, r1);
    }
}

fn gen_db(trees: &[&str], len: usize) -> (HashMap<Vec<u8>, Vec<u8>>, Db) {
    let data = data::random_map(len);
    let ds = crate::open("ds").unwrap();
    for &name in trees {
        let tree = ds.open_tree(name).unwrap();
        for (key, value) in &data {
            tree.insert(key, value.to_vec()).unwrap();
        }
    }
    (data, ds)
}
