use std::collections::HashMap;

use rand::{prelude::SliceRandom, thread_rng};

pub fn random_vec(len: usize) -> Vec<Vec<u8>> {
    let mut data = (0..len)
        .map(|i| {
            format!("{i:b}")
                .bytes()
                .map(|b| if b == b'1' { u8::MAX } else { u8::MIN })
                .collect()
        })
        .collect::<Vec<_>>();
    let mut rng = thread_rng();
    data.shuffle(&mut rng);
    data
}

pub fn random_map(len: usize) -> HashMap<Vec<u8>, Vec<u8>> {
    random_vec(len).into_iter().zip(random_vec(len)).collect()
}
